it21622
Data structures assignment 2020-2021
Harokopio University of Athens, Dept. of Informatics and Telematics.

http://www.dit.hua.gr/

Language level
------------------------
Java 11 or higher is required in order to compile and execute the project.

Compilation Instructions
------------------------

1. Install maven 
2. Change working directory to the directory containing pom.xml
3. Compile using maven

mvn package

3. Execute

Linux:
java -jar target/FrequencyMatrix.jar
java -jar target/HuffmanTreeBuilder.jar

Windows:
java -jar target\FrequencyMatrix.jar
java -jar target\HuffmanTreeBuilder.jar