package org.hua.HuffmanCoding;

import org.hua.BitHandling.BitVector;
import org.hua.huffmanTree.Tree;

/**
 * Class for decoding text which was previously encoded with Huffman code.
 * In order to use the decoder, provide the Huffman tree whose code used in the
 * encoding process.
 */
public class HuffmanDecoder {
    private final Tree huffmanTree;

    /**
     * Base constructor.
     * @param huffmanTree Huffman tree which will be used for the decoding
     */
    public HuffmanDecoder(Tree huffmanTree){
        this.huffmanTree = huffmanTree;
    }

    public String decode(EncodedFile encodedFile) {
        var decodedText = new StringBuilder();
        var bitVector = new BitVector(encodedFile.getBytes());
        var encodedInput = bitVector.getBitVector();
        var curNode = huffmanTree.root();
        for (var bitIndex = 0; bitIndex < encodedFile.getTotalNumberOfBits(); bitIndex++){
            var bit = encodedInput[bitIndex];

            if(bit)
                curNode = curNode.getRightChild();
            else
                curNode = curNode.getLeftChild();

            if(huffmanTree.isExternal(curNode)){
                decodedText.append(curNode.getSymbol().charValue());
                curNode = huffmanTree.root();
            }

        }
        return decodedText.toString();
    }
}
