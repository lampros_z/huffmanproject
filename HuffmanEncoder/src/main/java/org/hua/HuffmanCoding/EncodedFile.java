package org.hua.HuffmanCoding;

/**
 * A wrapper class for the encoded file that the Huffman encoder produced.
 * This class contains all the necessary information to help persist as well as read from disk an encoded file.
 */
public class EncodedFile {
    /**
     * The total number of bits that the file consists of.
     */
    private final int totalNumberOfBits;
    /**
     * The byte values(content) of the file.
     */
    private final byte[] bytes;

    /**
     * Base constructor needed to create an object of this class.
     * @param totalNumberOfBits The total number of bits of the encoded file(Length of bit vector)
     * @param bytes The byte values of the encoded file
     */
    public EncodedFile(int totalNumberOfBits, byte[] bytes) {
        this.totalNumberOfBits = totalNumberOfBits;
        this.bytes = bytes;
    }

    public int getTotalNumberOfBits() {
        return totalNumberOfBits;
    }

    public byte[] getBytes() {
        return bytes;
    }

    /**
     * Helper method that given the total number of bits of the file, returns the total number of bytes
     * needed to represent the file in memory.
     * @param totalNumberOfBits The total number of bits of encoded file
     * @return The total number of bytes needed to represent the whole file.
     */
    public static int calculateNumberOfBytes(int totalNumberOfBits){
        var bytes = totalNumberOfBits / 8;
        var remainder  = totalNumberOfBits % 8;
        if(remainder != 0)
            ++bytes;
        return bytes;
    }
}
