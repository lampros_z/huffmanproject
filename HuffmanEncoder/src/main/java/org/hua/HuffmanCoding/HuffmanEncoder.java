package org.hua.HuffmanCoding;

import org.hua.BitHandling.BitVector;

/**
 * Class for compressing text values using Huffman coding based on the first 128 ASCII characters
 */
public class HuffmanEncoder {
    /**
    * Number of ASCII characters supported
    */
    private static final int NUMBER_OF_ASCII_CHARACTERS = 128;
    /**
     * BitVector data structure used to represent a bit sequence.
     */
    private final BitVector bitVector;
    /**
     * Huffman code values of the first 128 ASCII characters.
     */
    private final String[] huffmanCode;

    /**
     * Base constructor used in order to create a Huffman encoder object instance.
     * @param huffmanCode A String array containing the Huffman code values for the first 128 ASCII characters
     */
    public HuffmanEncoder(String[] huffmanCode) {
        this.bitVector = new BitVector();
        this.huffmanCode = huffmanCode;
    }

    /**
     * Encode text using Huffman code.
     * @param text Text value to be encoded
     * @return Encoded text as byte array
     */
    public byte[] encode(String text){
        for (var charIndex = 0; charIndex < text.length(); charIndex++ )
            appendCharacterToBitset(text.charAt(charIndex));
        return bitVector.toByteArray();
    }

    /**
     * Helper method that appends an ASCII character to Bit Vector data structure.
     * @param character An ASCII character
     */
    private void appendCharacterToBitset(char character){
        var asciiCharacter = (int)character;
        if(asciiCharacter < 0 || asciiCharacter > NUMBER_OF_ASCII_CHARACTERS - 1)
            return;
        var code = huffmanCode[asciiCharacter];
        bitVector.appendBits(code);
    }

    /**
     * Returns the total number of bits of the encoded text.
     * @return The total number of bits of the encoded text
     */
    public int getBitVectorLength(){
        return bitVector.length();
    }

}
