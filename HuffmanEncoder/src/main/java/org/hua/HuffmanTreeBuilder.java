package org.hua;

import org.hua.fileIO.BinaryFileIO;
import org.hua.fileIO.TextFileIO;
import org.hua.huffmanTree.HuffmanTree;

/**
 * Driver class containing the main method
 * Read frequencies file and build the Huffman tree for
 * the character frequencies given.
 */
public class HuffmanTreeBuilder {
    /*
        Character frequencies file
    */
    public static final String FREQUENCIES_FILE = "frequencies.dat";
   /*
        Huffman tree file
    */
    public static final String TREE_FILE = "tree.dat";

    public static void main(String... args) {
        var textFileIO = new TextFileIO();
        /* Read frequencies from file*/

        var frequencies = textFileIO.readFrequencies(FREQUENCIES_FILE);
        /* Build Huffman tree */
        var huffmanTree = new HuffmanTree(frequencies);
        /* Write Huffman tree to stream(binary file)*/
        if( !huffmanTree.isEmpty() ){
            var bFileIO = new BinaryFileIO();
            System.out.println("Writing tree to file");
            bFileIO.writeTree(huffmanTree, TREE_FILE);
            System.out.println("Completed");
        }
    }

}
