package org.hua;

import org.hua.HuffmanCoding.EncodedFile;
import org.hua.fileIO.BinaryFileIO;
import org.hua.fileIO.TextFileIO;
/**
 * Driver class containing the main method.
 * This class is responsible for encoding the text file given as an argument,
 * using the Huffman code specified.
 */
public class HuffmanEncoder {

    public static final String HUFFMAN_CODE = "codes.dat";

    public static void main(String... args) {
        /*Encoding*/
        if( args.length != 2 ){
            System.out.println("Usage: java -jar HuffmanEncoder.jar <inputfile> <outputfile>");
            System.exit(1);
        }
        var inputFile = args[0];
        var outputFile = args[1];

        var textFileIO = new TextFileIO();
        /*Load text file to memory*/
        var textFile = textFileIO.readTextFile(inputFile);
        /*Load huffman code for the first 128 ASCII characters*/
        var code = textFileIO.readHuffmanCode(HUFFMAN_CODE);
        System.out.println("### Encoding ###");
        /*Initialize encoder*/
        var encoder = new org.hua.HuffmanCoding.HuffmanEncoder(code);
        /*Encode text file*/
        var encodedBook = encoder.encode(textFile);
        /*Wrap encoded text file with EncodedFile class in order to persist easily to disk*/
        var encodedFile = new EncodedFile(encoder.getBitVectorLength(), encodedBook);
        System.out.println("Writing file to disk...");
        /*Save file to permanent storage*/
        new BinaryFileIO().writeEncodedFile(encodedFile, outputFile);
        System.out.println("Finished");
    }
}
