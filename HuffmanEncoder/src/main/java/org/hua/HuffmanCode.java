package org.hua;

import org.hua.HuffmanCoding.EncodedFile;
import org.hua.HuffmanCoding.HuffmanDecoder;
import org.hua.HuffmanCoding.HuffmanEncoder;
import org.hua.fileIO.BinaryFileIO;
import org.hua.fileIO.TextFileIO;

/**
 * Driver class containing the main method.
 * This class is responsible for reading a Huffman tree from a file
 * and then calculating the Huffman code for each character by
 * traversing the Huffman tree.
 */
public class HuffmanCode {

    public static final String TREE_FILE = "tree.dat";
    public static final String CODE_FILE = "codes.dat";

    public static void main(String... args) {
        /* Read tree from file */
        var huffmanTree = new BinaryFileIO().readTree(TREE_FILE);
        var code = huffmanTree.getHuffmanCode();
        System.out.println("### Writing huffman code to file ###");
        new TextFileIO().writeArray(code, CODE_FILE);
        System.out.println("### Finished ###");

    }

}
