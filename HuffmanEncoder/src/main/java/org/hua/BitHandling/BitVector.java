package org.hua.BitHandling;

import java.util.ArrayList;
import java.util.List;

/**
 * A class representing a sequence of bits. In order to represent bit values the boolean data type is used.
 * This class provides functionality for converting between a byte array to boolean(bit) array and vice versa.
 * The ArrayList data structure is used in order to support the functionality of this class.
 */
public class BitVector {
    /**
        Initial capacity value
     */
    private static final int VECTOR_CAPACITY = 8_192;
    /**
     * A List containing boolean(bit) values.
     */
    private final List<Boolean> bitVector;

    /**
     * Default constructor initializing an Array list
     * data structure with initial capacity of 8_192
     * in order to support the functionality of the bit vector.
     */
    public BitVector() {
        bitVector = new ArrayList<>(VECTOR_CAPACITY);
    }

    /**
     * Given a byte array create a boolean(bit) value representation using an ArrayList with
     * initial capacity the total number of bits(bytes*8=numBits) plus one in order to optimize the ArrayList
     * data structure and avoid resizing operation many times.
     * @param bytes A byte array
     */
    public BitVector(byte[] bytes){
        bitVector = new ArrayList<>((bytes.length*8)+1);
        for (byte aByte : bytes) {
            bitVector.add((aByte & 0x80) != 0);
            bitVector.add((aByte & 0x40) != 0);
            bitVector.add((aByte & 0x20) != 0);
            bitVector.add((aByte & 0x10) != 0);
            bitVector.add((aByte & 0x8) != 0);
            bitVector.add((aByte & 0x4) != 0);
            bitVector.add((aByte & 0x2) != 0);
            bitVector.add((aByte & 0x1) != 0);
        }
    }

    /**
     * Given a bit sequence as a String representation, append each bit in the BitVector.
     * @param bitStream A string representation of a bit sequence. For instance "10100"
     */
    public void appendBits(String bitStream){
        for (int i = 0; i < bitStream.length(); i++) {
            if(bitStream.charAt(i) == '1' ) bitVector.add(true);
            else bitVector.add(false);
        }
    }

    /**
     * Transform the BitVector into a byte array. If the length of the bit vector is not evenly divisible, zeros
     * are appended(padding) in order to make the length evenly divisible.
     * @return An array of bytes
     */
    public byte[] toByteArray(){
        var remainder = bitVector.size() % 8;

        if( remainder != 0){
            for (var i = 0; i < (8 - remainder) ; i++) {
                bitVector.add(false);
            }
        }

        var byteArray = new byte[bitVector.size() / 8];
        for (var i = 0; i < byteArray.length; i++) {
            var byteChunk = i*8;
            var byteValue = (byte)(
                    (bitVector.get(byteChunk    ) ? 1 << 7 : 0) +
                    (bitVector.get(byteChunk + 1) ? 1 << 6 : 0) +
                    (bitVector.get(byteChunk + 2) ? 1 << 5 : 0) +
                    (bitVector.get(byteChunk + 3) ? 1 << 4 : 0) +
                    (bitVector.get(byteChunk + 4) ? 1 << 3 : 0) +
                    (bitVector.get(byteChunk + 5) ? 1 << 2 : 0) +
                    (bitVector.get(byteChunk + 6) ? 1 << 1 : 0) +
                    (bitVector.get(byteChunk + 7) ? 1 : 0)
                    );
            byteArray[i] = byteValue;
        }

        if( remainder != 0){
            for (var i = 0; i < (8 - remainder) ; i++) {
                bitVector.remove(bitVector.size()-1);
            }
        }
        return byteArray;
    }

    /**
     * Getter method that represents the BitVector as a Boolean array.
     * @return The BitVector as a Boolean array
     */
    public Boolean[] getBitVector(){
        return bitVector.toArray(new Boolean[0]);
    }

    /**
     * Clear all stored values of the Bit Vector.
     */
    public void clear(){
        bitVector.clear();
    }

    /**
     * Return the length of the Bit Vector in other words, the total
     * number of bits contained.
     * @return The length of the Bit Vector
     */
    public int length(){return bitVector.size();}

}
