package org.hua;

import org.hua.characterUtils.CharStatistics;
import org.hua.fileIO.FileDownloader;
import org.hua.fileIO.TextFileIO;

/**
 * Driver class containing the main method.
 * It is responsible for calculating a frequency matrix for the first 128 ASCII
 * characters. Three large text files from the web are used as resources.
 * Frequency matrix is stored in a text file called frequencies.dat.
 */
public class FrequencyMatrix
{
    /*
        urls of text books.
     */
    public static final String BOOK_1_URL = "https://www.gutenberg.org/files/1342/1342-0.txt";
    public static final String BOOK_2_URL = "https://www.gutenberg.org/files/11/11-0.txt";
    public static final String BOOK_3_URL = "https://www.gutenberg.org/files/2701/2701-0.txt";
    /*
        Text book file names
     */
    public static final String BOOK_1 = "1342-0.txt";
    public static final String BOOK_2 = "11-0.txt";
    public static final String BOOK_3 = "2701-0.txt";
    /*
        Frequency matrix file
     */
    public static final String OUTPUT_FILE = "frequencies.dat";

    public static void main( String... args )

    {
        /*Download files if they do not exist on the file system*/
        var fileDownloader = new FileDownloader();
        fileDownloader.downloadTextFile(BOOK_1_URL, BOOK_1);
        fileDownloader.downloadTextFile(BOOK_2_URL, BOOK_2);
        fileDownloader.downloadTextFile(BOOK_3_URL, BOOK_3);
        /*Read text book files form the file system*/
        var textFileIO = new TextFileIO();
        var book1 = textFileIO.readTextFile(BOOK_1);
        var book2 = textFileIO.readTextFile(BOOK_2);
        var book3 = textFileIO.readTextFile(BOOK_3);
        /*Calculate first 128 ASCII character frequencies*/
        var charStatistics = new CharStatistics();
        var ASCICharactersFrequencies = charStatistics.calculateASCIICharacterFrequencies(book1, book2, book3);
        /*Write frequencies to a text file called frequencies.dat*/
        System.out.println("Writing frequencies to file....");
        textFileIO.writeArray(ASCICharactersFrequencies, OUTPUT_FILE);
        System.out.println("Completed!");
    }

}
