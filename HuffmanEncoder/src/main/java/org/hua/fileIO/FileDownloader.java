package org.hua.fileIO;

import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Download files from the web
 */
public class FileDownloader {
    /**
     * Download file from the url provided, only if
     * the specified file does not exist on the file system.
     * @param url The url of the file
     * @param filename The path where the downloaded file should be saved
     */
    public void downloadTextFile(String url, String filename){
        try{
            var file = new File(filename);
            if(!file.exists()) {
                System.out.println( "Downloading file: " + file.getName() );
                FileUtils.copyURLToFile(new URL(url), file);
            }
        }catch ( IOException e ) {
            System.err.println("Download error: " + e.getMessage());
        }catch ( NullPointerException e ) {
            System.err.println("Invalid filename argument: " + e.getMessage());
        }
    }

}
