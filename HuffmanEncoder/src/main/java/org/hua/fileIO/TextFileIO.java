package org.hua.fileIO;

import java.io.*;

/**
 * TextFileIO class is responsible for handling input and output of text files,
 * using the BufferedReader and BufferedWriter classes for efficient
 * reading and writing. 
 */
public class TextFileIO {
    /**
     * Number of ASCII characters supported
     */
    private static final int NUMBER_OF_ASCII_CHARACTERS = 128;
    /**
     * Buffer size
     */
    private static final int BUFFER_SIZE = 8_192;
    private String operatingSystem;


    /**
     * A method responsible for reading the contents of the text file
     * specified by the filename parameter
     * @param filename File path
     * @return The content of the text file
     */
    public String readTextFile(String filename) {
        var textString = new StringBuilder();
        try( var bufferedReader = new BufferedReader(new FileReader(filename), BUFFER_SIZE) ){
            var inputCharacter = -1;
            while( (inputCharacter = bufferedReader.read()) != -1 ) {
                var character = (char)inputCharacter;
                textString.append(character);
            }
        } catch(FileNotFoundException e){
            System.err.println("File " + filename + " does not exist.");
        }catch(IOException e){
            System.err.println("Error reading from file.");
        }
        return textString.toString();
    }

    /**
     * Writes into a text file the contents of the given array

     * @param array An array containing information
     * @param filename File path
     */
    public <E> void  writeArray(E[] array, String filename){
        try ( var writer = new BufferedWriter(new FileWriter(filename), BUFFER_SIZE) ){
            for (var charIndex = 0; charIndex < array.length; charIndex++) {
                var line = charIndex + ","+ array[charIndex] + "\n";
                writer.write(line);
            }
        }catch(FileNotFoundException e){
            System.err.println("File " + filename + " cannot be opened for writing.");
        }catch(IOException e){
            System.err.println("Error writing to file.");
        }
    }

    /**
     * Read frequencies of the first 128 ASCII characters from text file
     * @param filename File path
     * @return An array containing the frequencies of the first 128 ASCII characters
     */
    public int[] readFrequencies(String filename) {
        var frequencies = new int[NUMBER_OF_ASCII_CHARACTERS];
        try ( var bufferedReader = new BufferedReader( new FileReader(filename) , BUFFER_SIZE) ){

            var inputLine = "";
            while( (inputLine = bufferedReader.readLine()) != null ){
                var tokens = inputLine.split(",");
                try{
                    var character = Integer.parseInt(tokens[0]);
                    var frequency = Integer.parseInt(tokens[1]);
                    frequencies[character] = frequency;
                }catch(NumberFormatException e){
                    System.err.println("String does not contain a parsable integer.");
                }
            }

        } catch(FileNotFoundException e){
            System.err.println("File " + filename + " does not exist.");
        }catch(IOException e){
            System.err.println("Error reading from file.");
        }
        return frequencies;
    }

    public String[] readHuffmanCode(String filename) {
        var codes = new String[NUMBER_OF_ASCII_CHARACTERS];
        try ( var bufferedReader = new BufferedReader( new FileReader(filename) , BUFFER_SIZE) ){

            var inputLine = "";
            while( (inputLine = bufferedReader.readLine()) != null ){
                var tokens = inputLine.split(",");
                try{
                    var character = Integer.parseInt(tokens[0]);
                    var code = tokens[1];
                    codes[character] = code;
                }catch(NumberFormatException e){
                    System.err.println("String does not contain a parsable integer.");
                }
            }

        } catch(FileNotFoundException e){
            System.err.println("File " + filename + " does not exist.");
        }catch(IOException e){
            System.err.println("Error reading from file.");
        }
        return codes;
    }

    /**
     * Write string value into a text file
     * @param str A string value containing valuable information
     * @param filename File path where data will be stored
     */
    public void writeText(String str, String filename){
        try ( var writer = new BufferedWriter(new FileWriter(filename), BUFFER_SIZE) ){
            writer.write(str);
        }catch(FileNotFoundException e){
            System.err.println("File " + filename + " cannot be opened for writing.");
        }catch(IOException e){
            System.err.println("Error writing to file.");
        }
    }

}