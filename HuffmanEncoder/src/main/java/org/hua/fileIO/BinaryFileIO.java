package org.hua.fileIO;

import java.io.*;

import org.hua.HuffmanCoding.EncodedFile;
import org.hua.huffmanTree.HuffmanTree;

/**
 *  BinaryFileIO class is responsible for handling input and output of binary files,
 *  using the BufferedInputStream and BufferedOutputStream classes for efficient
 *  reading and writing.
 */
public class BinaryFileIO {

    /**
     * Constant specifying buffered streams' buffer size
     */
    private static final int BUFFER_SIZE = 16_384;

    /**
     * Persist Huffman tree object to stream(binary file).
     * @param tree Huffman tree object to be persisted
     * @param filename File path
     */
    public void writeTree(HuffmanTree tree, String filename) {
        try ( var outputStream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(filename), BUFFER_SIZE ) ) ) {
            outputStream.writeObject(tree);
        } catch (FileNotFoundException e) {
            System.err.println("File " + filename + " cannot be opened for writing: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Error writing to file: " + e.getMessage());
        }
    }

    /**
     * Read Huffman tree object from stream(binary file).
     * @param filename File path
     * @return Huffman tree object that read from the file path or null if error occurred.
     */
    public HuffmanTree readTree(String filename) {
        try ( var inputStream = new ObjectInputStream( new BufferedInputStream( new FileInputStream(filename) , BUFFER_SIZE) ) ) {
            return (HuffmanTree) inputStream.readObject();
        } catch (FileNotFoundException e) {
            System.err.println("File " + filename + " does not exist: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Error reading from file: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.err.println("Class not found exception: " + e.getMessage());
        }
        return null;
    }

    /**
     * Write an encoded file to disk. This method writes at the start of the file the total number of bits that the encoded file consists of.
     * Then all  the bytes of the encoded file are written to the file specified.
     * @param encodedFile An object containing all the necessary information to persist the encoded file to disk
     * @param filename File path
     */
    public void writeEncodedFile(EncodedFile encodedFile, String filename){
        try ( var outputStream = new DataOutputStream ( new BufferedOutputStream( new FileOutputStream(filename), BUFFER_SIZE ) ) ) {
            outputStream.writeInt(encodedFile.getTotalNumberOfBits());
            if(encodedFile.getBytes() == null){
                System.err.println("Encoded file is empty");
                return;
            }
            outputStream.write(encodedFile.getBytes());
        } catch (FileNotFoundException e) {
            System.err.println("Cannot open " + filename + " for writing: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Error writing to file: " + e.getMessage());
        }
    }

    /**
     * Read an encoded file from disk. This method reads at the top of the specified file the total number of bits and then
     * reads n number of bytes.
     * @param filename File path
     * @return An encoded file object containing the total number of bits of the encoded file as well as the bytes of the file
     */
    public EncodedFile readEncodedFile(String filename){
        try ( var inputStream = new DataInputStream ( new BufferedInputStream( new FileInputStream(filename), BUFFER_SIZE ) ) ) {
            var totalNumberOfBits = inputStream.readInt();
            var numberOfBytesToRead = EncodedFile.calculateNumberOfBytes(totalNumberOfBits);
            var content = inputStream.readNBytes(numberOfBytesToRead);
            return new EncodedFile(totalNumberOfBits, content);
        } catch (FileNotFoundException e) {
            System.err.println("File " + filename + " does not exist: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Error reading from file: " + e.getMessage());
        }
        return null;
    }

}
