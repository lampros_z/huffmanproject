package org.hua.characterUtils;

import java.util.Arrays;

/**
 * Calculate statistics for strings
 */
public class CharStatistics {
    /**
     * Number of ASCII characters supported
     */
    private static final int NUMBER_OF_ASCII_CHARACTERS = 128;
    /**
     *
      * @param args A variable number of string frequencies
     *  @return An int array containing the sum of frequencies of the first 128 ASCII
     *  characters of the given string values.
     */
    public Integer[] calculateASCIICharacterFrequencies(String... args){
        var frequencies = initIntegerArray(NUMBER_OF_ASCII_CHARACTERS);

        for(var arg : args){
            var results = calculateCharacterFrequencies(arg);
            horizontalSum(frequencies, results);
        }

        return frequencies;
    }

    /**
     * Given a string value calculate the first 128 ASCII character
     * frequencies.
     * @param str A string value
     * @return A frequency matrix of the first 128 ASCII characters
     */
    private Integer[] calculateCharacterFrequencies(String str){
        var frequencies = initIntegerArray(NUMBER_OF_ASCII_CHARACTERS);
        for(var index=0; index < str.length(); index++) {
            var character = (int)str.charAt(index);
            if( character >= 0 && character <= NUMBER_OF_ASCII_CHARACTERS -1 )
                frequencies[character]++;
        }

        return frequencies;
    }

    /**
     * Sum each row of source array with target array and store results in target.
     * Since arrays in java are passed by reference no need for return value is needed.
     * Horizontal sum: Sum each row of the arrays given.
     * For instance if array1 = { 1 1 1 } and array2 = { 0 2 3 } then horizontalSum(array1, array2) will
     * transform array1 to array1 = { 1 3 4 }.
     * @param target The integer array where the results of the horizontal sum will be stored
     * @param source An integer array which values will be added to the values of source array.
     */
    private void horizontalSum(Integer[] target, Integer[] source){
        if(target.length != source.length)
            throw new IllegalArgumentException("Length of source array and target array are diferrent.");
        for (var i = 0; i < target.length; i++) {
            target[i] += source[i];
        }
    }

    /**
     * Initialize Integer array with zeros
     * @param length length of the array
     */
    private Integer[] initIntegerArray(int length){
        var array = new Integer[length];
        Arrays.fill(array, 0);
        return array;
    }

}
