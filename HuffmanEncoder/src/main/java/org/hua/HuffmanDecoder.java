package org.hua;

import org.hua.fileIO.BinaryFileIO;
import org.hua.fileIO.TextFileIO;
/**
 * Driver class containing the main method.
 * This class is responsible for decoding the text file given as an argument,
 * using the Huffman tree specified.
 */
public class HuffmanDecoder {

    public static final String HUFFMAN_TREE = "tree.dat";

    public static void main(String[] args) {
        /*Decoding*/
        if( args.length != 2 ){
            System.out.println("Usage: java -jar HuffmanDecoder.jar <inputfile> <outputfile>");
            System.exit(1);
        }
        var inputFile = args[0];
        var outputFile = args[1];

        var bFileIO = new BinaryFileIO();
        /*Load Huffman tree*/
        var huffmanTree = bFileIO.readTree(HUFFMAN_TREE);
        /*Create Huffman decoder instance*/
        var decoder = new org.hua.HuffmanCoding.HuffmanDecoder(huffmanTree);
        /*Load encoded file*/
        var file = bFileIO.readEncodedFile(inputFile);
        System.out.println("### Decoding ###");
        /*Decode encoded file*/
        var textFile = decoder.decode(file);
        System.out.println("Writing file to disk...");
        /*Save decoded file to permanent storage*/
        new TextFileIO().writeText(textFile, outputFile);
        System.out.println("Finished");
    }
}
