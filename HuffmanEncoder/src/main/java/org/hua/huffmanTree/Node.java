package org.hua.huffmanTree;

import java.io.Serializable;

/**
 * A class tha models the node of a tree data structure.
 * Objects of this class, hold all the necessary information
 * needed to build a Huffman tree
 */
public class Node implements Comparable<Node>, Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * An ASCII character
     */
    private Character symbol;
    /**
     * Frequency of the ASCII character
     */
    private int frequency;
    /**
     * Null if this node is external to the tree
     * It's left child node if this node is internal to the tree
     */
    private Node leftChild;
    /**
     * Null if this node is external to the tree
     * It's right child node if this node is internal to the tree
     */
    private Node rightChild;

    /**
     * Construct an external node of the Huffman tree object containing information
     * only for the ASCII character and it's frequency
     * @param symbol An ASCII character
     * @param frequency Frequency of the given ASCII character
     */
    public Node(char symbol, int frequency) {
        this(symbol, frequency, null, null);
    }

    /**
     * Construct an internal node of the Huffman tree containing information
     * for the total frequency at the current level as well as the left and right
     * child of this node
     * @param frequency Total frequency up this level
     * @param leftChild Left node child
     * @param rightChild Right node child
     */
    public Node(int frequency, Node leftChild, Node rightChild) {
        this(null, frequency, leftChild, rightChild);
    }

    /**
     * Basic constructor for build node objects
     * @param symbol An ASCII character
     * @param frequency The frequency of the given ASCII character
     * @param leftChild Left child of this node or null if this is an external node
     * @param rightChild Right child of this node or null if this is an external node
     */
    public Node(Character symbol, int frequency, Node leftChild, Node rightChild) {
        this.symbol = symbol;
        this.frequency = frequency;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }

    /**
     * Getter method for symbol field
     * @return the symbol stores in this object
     */
    public Character getSymbol() {
        return symbol;
    }

    /**
     * Setter method for symbol field
     * @param symbol An ASCII character
     */
    public void setSymbol(Character symbol) {
        this.symbol = symbol;
    }

    /**
     * Getter method for frequency filed
     * @return the frequency that corresponds to this node object
     */
    public int getFrequency() {
        return frequency;
    }

    /**
     * Setter method for frequency field
     * @param frequency Frequency value
     */
    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    /**
     * Getter method for the left child field
     * @return reference to the left child node
     */
    public Node getLeftChild() {
        return leftChild;
    }

    /**
     * Setter method for left child field
     * @param leftChild The new left child node
     */
    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    /**
     * Getter method for the right child field
     * @return reference to the right child node
     */
    public Node getRightChild() {
        return rightChild;
    }

    /**
     * Setter method for right child field
     * @param rightChild The new right child node
     */
    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    /**
     * Compare node objects according to their frequency field
     * Returns 0 if the nodes are equal
     * Returns greater that zero value if this node is greater than the given node
     * Returns less than zero value if this node is smaller than the given node
     * @param o Node object
     * @return  Comparison result
     */
    @Override
    public int compareTo(Node o) {
        if(o == null)
            throw new IllegalArgumentException("Node of Huffman tree cannot be null.");
        if(this == o)
            return 0;
        return Integer.compare(this.frequency, o.frequency);
    }

}
