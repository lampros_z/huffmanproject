package org.hua.huffmanTree;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;

/**
 * Huffman tree data structure
 */
public class HuffmanTree implements Tree, Serializable {
    /**
     * Number of ASCII characters supported
     */
    private static final int NUMBER_OF_ASCII_CHARACTERS = 128;
    private static final long serialVersionUID = 1L;
    /**
     * The unique root node of this tree
     */
    private final Node root;

    private String[] huffmanCode;

    /**
     *  Construct huffman tree
     * @param frequencies Frequencies of the first 128 ASCII characters
     */
    public HuffmanTree(int[] frequencies) {
        //Initialised capacity to 129 in order to avoid resizing of the priority queue
        var treeNodes = new PriorityQueue<Node>(NUMBER_OF_ASCII_CHARACTERS + 1);

        for(int character = 0; character < NUMBER_OF_ASCII_CHARACTERS; character++)
            treeNodes.add(new Node((char)character, frequencies[character]));

        while(treeNodes.size() > 1)
            treeNodes.add( mergeNodes( treeNodes.poll(), treeNodes.poll() ) );

        root = treeNodes.poll();
    }

    @Override
    public Node root() {
        return root;
    }

    @Override
    public boolean isInternal(Node node) {
        return node.getSymbol() == null;
    }

    @Override
    public boolean isExternal(Node node) {
        return node.getSymbol() != null;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public int height(Node node){
        if (node == null)
            return 0;
        else
        {
            var leftHeight = height(node.getLeftChild());
            var rightHeight = height(node.getRightChild());

            if (leftHeight > rightHeight)
                return(leftHeight+1);
            else return(rightHeight+1);
        }
    }

    /**
     * Get the Huffman code that corresponds to this tree
     * @return A String array where the indices of the array represent the character and the values contain the Huffman code.
     */
    public String[] getHuffmanCode(){
        if(huffmanCode == null)
            huffmanCode = generateHuffmanCode();
        return huffmanCode;
    }

    /**
     * Given two nodes, adds their frequencies and returns a new node object
     * with frequency the result of the addition and sets as left and right child
     * the two nodes given an parameters
     * @param nodeLeft A node of this tree
     * @param nodeRight A node of this tree
     * @return Merged node
     */
    private Node mergeNodes(Node nodeLeft, Node nodeRight){
        if( nodeLeft == null || nodeRight == null )
            throw new NullPointerException("Cannot merge null nodes");
        if( nodeLeft == nodeRight )
            throw new IllegalArgumentException("Cannot merge the same node");

        var totalFrequency = nodeLeft.getFrequency() + nodeRight.getFrequency();
        return new Node(totalFrequency, nodeLeft, nodeRight);
    }


    /**
     * Helper method that traverses the Huffman tree and produces the Huffman code for the first 128 ASCII characters.
     * If Huffman tree is empty an exception with proper message is thrown.
     * If Huffman tree contains only one node then code "0" is returned for the specified character.
     *
     * @return A string array containing the Huffman code for the first 128 ASCII characters. The indices of the array represent the ASCII character and the values contain the Huffman code.
     */
    private String[] generateHuffmanCode(){

        var coding = new String[NUMBER_OF_ASCII_CHARACTERS];
        var path = new ArrayDeque<StringBuilder>();
        var root = root();

        if( isEmpty() ){
            throw new NoSuchElementException("Cannot generate the Huffman code of an empty tree");
        }

        if( root.getLeftChild() == null && root.getRightChild() == null ){
            if(root.getSymbol() == null)
                throw new IllegalArgumentException("Huffman tree must contain at least one character");
            var asciiCharacterDec = (int)root.getSymbol();
            checkIfAsciiCharacterIsInRange(asciiCharacterDec);
            coding[asciiCharacterDec] = "0";
            return coding;
        }

        var stack = new ArrayDeque<Node>();
        stack.push(root);
        var localPath = new StringBuilder();
        while ( stack.size() != 0 ){
            var node = stack.pop();

            if( !path.isEmpty() )
                localPath = path.pop();

            if( isExternal(node) ){
                var asciiCharacterDec = (int)node.getSymbol();
                checkIfAsciiCharacterIsInRange(asciiCharacterDec);
                coding[asciiCharacterDec] = localPath.toString();
                continue;
            }

            if( node.getRightChild() != null ){
                stack.push(node.getRightChild());
                if(node == root)
                    path.push(new StringBuilder("1"));
                else
                    path.push(new StringBuilder(localPath.toString()).append('1'));
            }

            if( node.getLeftChild() != null ){
                stack.push(node.getLeftChild());
                if(node == root)
                    path.push(new StringBuilder("0"));
                else
                    path.push(new StringBuilder(localPath.toString()).append('0'));
            }

        }
        return coding;
    }

    /**
     * Helper method to check if the decimal representation of the character is in the accepted range.
     * @param asciiCharacter The decimal value of the ASCII character
     */
    private void checkIfAsciiCharacterIsInRange(int asciiCharacter){
        if(asciiCharacter < 0 || asciiCharacter > NUMBER_OF_ASCII_CHARACTERS - 1)
            throw new IllegalArgumentException("Only ASCII characters from 0 up to 127 are allowed");
    }

}
