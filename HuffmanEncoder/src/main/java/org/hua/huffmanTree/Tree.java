package org.hua.huffmanTree;

/**
 * Tree data structure interface
 */
public interface Tree {
    /**
     * This method simply returns a reference to
     * the root node of this tree.
     * @return The unique root node of the tree
     */
    Node root();

    /**
     * Determines if a node is internal to this tree
     * @param node A node of this tree
     * @return true if the node is internal
     */
    boolean isInternal(Node node);

    /**
     * Determines if a node is external to this tree
     * @param node A node of this tree
     * @return true if the node is external
     */
    boolean isExternal(Node node);

    /**
     * Returns true  if this tree is empty
     * Returns false if this tree is not empty
     * @return true  if this tree is empty
     */
    boolean isEmpty();

    /**
     * Returns the height of the given node
     * @param node A node of this tree
     * @return The height of the
     */
    int height(Node node);
}
